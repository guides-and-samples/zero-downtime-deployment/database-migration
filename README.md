# Database migration

Guides and samples of Zero Downtime deployment - DB migration.

# Content

## Table operations
- [Create Table](table/create-table.md)
- [Remove Table](table/remove-table.md)
- [Rename Table](table/rename-table.md)

## Column operations
- [Create Column](column/create-column.md)
- [Rename Column](column/rename-column.md)
- [Remove Column](column/remove-column.md)
- [Change Column Type](column/change-column-type.md)
- [Update Column Content](column/update-column-content.md)
