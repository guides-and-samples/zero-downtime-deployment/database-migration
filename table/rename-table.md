# Rename Column
>This page describes how to safely rename a DB table.

# Scenario
We have a table called `table_a` which we want to rename to `table_b`.

# Migration steps

## Current release
1. Current version of a service:
    -  Uses `table_a`

## Release 1
1. Pre deployment script:
    - Create `table_b` with the same columns as in `table_a`
        ```sql
        CREATE TABLE table_b (
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR
        );
        ```
2. New version of a service:
    - Writes to `table_a` as well as `table_b` while uses the same `id` for both tables
    - Reads from `table_a` as well as `table_b`

## Release 2
1. Pre deployment script:
    - Migrate data from `table_a` to `table_b`
        ```sql
        INSERT INTO table_a(id, name) 
        SELECT b.id, b.name 
        FROM table_b 
        WHERE NOT EXISTS (SELECT 1 FROM table_a a WHERE a.id = b.id);
        ```
2. New Version of a service:
    - Writes to `table_b`
    - Reads from `table_b`

## Release 3
1. Pre deployment script:
    - Removes `table_a`
        ```sql
        DROP TABLE table_a;
        ```
