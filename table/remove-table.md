# Remove Column
>This page describes how to safely remove a DB column.

# Scenario
We have a table called `table_a` which we want to remove.

# Migration steps

## Current release
1. Current version of a service:
    - Uses to `table_a`

## Release 1
1. New version of a service:
    - Doesn't use `table_a`

## Release 2
1. Pre deployment script:
    - Remove `table_a`
        ```sql
        DROP TABLE table_a;
        ```
