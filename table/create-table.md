# Create Table
>This page describes how to safely create a DB table.

# Scenario
We want to create a new table called `table_a` with using `UUID` as primary key.

# Migration steps

## Release 1
1. Pre deployment script:
    - Create `table_a`
        ```sql
        CREATE TABLE table_a (
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR
        );
        ```
2. New version of a service:
    - Uses `table_a`
        ```java
        @Entity
        @Table(name = "table_a")
        public class TableA implements Serializable {

            @Id
            @GeneratedValue(generator = "UUID")
            @GenericGenerator(
                name = "UUID",
                strategy = "org.hibernate.id.UUIDGenerator"
            )
            @Column(name = "id")
            private UUID id;

            @Column(name = "name")
            private String name;

            @Version
            @Column(name = "version")
            private Integer version;

            // getters and setters
        }
        ```
