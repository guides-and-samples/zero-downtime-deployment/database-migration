# Remove Column
>This page describes how to safely remove a DB column.

# Scenario
We have a column called `column_a` which we want to remove.

# Migration steps

## Current release
1. Current version of a service:
    - Writes to `column_a`
    - Reads from `column_a`

## Release 1
1. Pre deployment script:
    - Remove a `NON-NULL` constraint from `column_a`
        ```sql
        ALTER TABLE table_name ALTER COLUMN column_a DROP NOT NULL;
        ```
2. New version of a service:
    - Doesn't use `column_a`

## Release 2
1. Pre deployment script:
    - Remove `column_a`
        ```sql
        ALTER TABLE table_name DROP COLUMN column_a;
        ```
