# Create Column
>This page describes how to safely create a DB column.

# Scenario
We want to create a new column called `column_a` with `NON-NULL` constraint.

# Migration steps

## Release 1
1. Pre deployment script:
    - Create `column_a` without `NON-NULL` constraint from `column_a`
        ```sql
        ALTER TABLE table_name ADD COLUMN column_a VARCHAR;
        ```
2. New version of a service:
    - Uses `column_a`

## Release 2
1. Pre deployment script:
    - Update `null` values to default
        ```sql
        UPDATE table_name SET column_a = 'default_value' WHERE column_a IS NULL;
        ```
    - Add a `NON-NULL` constraint to `column_a`
        ```sql
        ALTER TABLE table_name ALTER COLUMN column_a SET NOT NULL;
        ```
