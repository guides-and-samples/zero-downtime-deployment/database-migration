# Rename Column
>This page describes how to safely rename a DB column.

# Scenario
We have a column called `column_a` which we want to rename to `column_b`.

# Migration steps

## Current release
1. Current version of a service:
    - Writes to `column_a`
    - Reads from `column_a`

## Release 1
1. Pre deployment script:
    - Remove a `NON-NULL` constraint from `column_a`
        ```sql
        ALTER TABLE table_name ALTER COLUMN column_a DROP NOT NULL;
        ```
    - Create `column_b`
        ```sql
        ALTER TABLE table_name ADD COLUMN column_b VARCHAR;
        ```
2. New version of a service:
    - Writes to `column_a` as well as `column_b`
        ```java
        public void setColumnB(final String columnB){
            this.columnA = this.columnB = columnB;
        }
        ```
    - Reads from `column_a` as well as `column_b` and returns the value of that one which is not `NULL`
        ```java
        public String getColumnB(){
            return notNull(columnA)
                ? columnA
                : columnB;
        }
        ```

## Release 2
1. Pre deployment script:
    - Migrate data from `column_a` to `column_b`
        ```sql
        UPDATE table_name SET column_b = column_a WHERE column_b IS NULL AND column_a IS NOT NULL;
        ```
2. New Version of a service:
    - Writes to `column_b`
    - Reads from `column_b`

## Release 3
1. Pre deployment script:
    - Remove `column_a`
        ```sql
        ALTER TABLE table_name DROP COLUMN column_a;
        ```
    - Add a `NON-NULL` constraint to `column_b`
        ```sql
        ALTER TABLE table_name ALTER COLUMN column_b SET NOT NULL;
        ```
